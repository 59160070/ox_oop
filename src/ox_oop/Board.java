package ox_oop;
public class Board {
    public Player x;
    public Player o;
    public Player currentPlayer;
    public int turnCount;
    
    public char[][] table = {
        {'-','-','-'},
        {'-','-','-'},
        {'-','-','-'}
    };
    
    public Board(Player x,Player o){
        this.x = x;
        this.o = o;
        currentPlayer = x;
    }
    
    public char[][] getTable(){
        return table;
    }
    
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    
    public boolean setTable(int row ,int col){
        if(table[row][col]=='-'){
            table[row][col] = currentPlayer.getName();
            return true;
        }
        return false;
    }
    
    public boolean checkRow(int row){
        for(int col=0;col<table[row].length;col++){
            if(table[row][col]!=currentPlayer.getName()){
                return false;
            }
        }
        return true;
    }
    
    public boolean checkRow(){
        if(checkRow(0)||checkRow(1)||checkRow(2)){
            System.out.println("winner line Row");
            return true;
        }
        return false;
    }
    
    public boolean checkCol(int col){
        for(int row=0;row<table.length;row++){
            if(table[row][col]!=currentPlayer.getName()){
                return false;
            }
        }
        return true;
    }
    
    public boolean checkCol(){
        if(checkCol(0)||checkCol(1)||checkCol(2)){
            System.out.println("winner line Column");
            return true;
        }
        return false;
    }
    
    public boolean checkDiagonal1(){
        for(int i=0;i<table.length;i++){
            if(table[i][i]!=currentPlayer.getName()){
                return false;
            }
        }
        System.out.println("winner line Diagonal1");
        return true;
    }
    
    public boolean checkDiagonal2(){
        for(int i=0;i<table.length;i++){
            if(table[2-i][i]!=currentPlayer.getName()){
                return false;
            }
        }
        System.out.println("winner line Diagonal2");
        return true;
    }
    
    public boolean checkDraw(){
        if(turnCount==8){
            System.out.println("Game Draw");
            return true;
        }
        return false;
    }
    
    public boolean checkWin(){
        if(checkRow()){            
            return true;
        }else if(checkCol()){
            return true;
        }else if(checkDiagonal1()){
            return true;
        }else if(checkDiagonal2()){
            return true;
        }else if(checkDraw()){
            return true;
        }
        return false;
    }
    
    public boolean isFinish(){        
        if(checkWin()){
            System.out.println("Winner");
            return true;
            
        }else if(checkDraw()){
            return true;
        }
        return false;
    }
    
    public void switchPlayer(){
        if(currentPlayer==x){
            currentPlayer = o;
        }else{
            currentPlayer = x;
        }
        turnCount++;
    }
}
