package ox_oop;

import java.util.Scanner;
public class Game {
    public Player x;
    public Player o;
    public Board board;
    
    public Game(){
        x = new Player('X');
        o = new Player('O');
        board = new Board(x,o);
    }
    
    public void play(){
        showWelcome();
        while(true){
            showTable();
            showTurn();
            input();
            if(board.isFinish()){
                break;
            }
            board.switchPlayer();
        }
    }
    
    public void showWelcome(){
        System.out.println("Welcome OX_Game");
    }
    
    public void showTable(){
        char[][] table = board.getTable();
        System.out.println("  1 2 3");
        for(int i=0;i<table.length;i++){
            System.out.print(i+1);
            for(int j=0;j<table[i].length;j++){
                System.out.print(" "+table[i][j]);
            }
            System.out.println();
        }
    }
    
    public void showTurn(){
        Player player = board.getCurrentPlayer();
        System.out.println(player.getName()+ " turn");
    }
    
    public void input(){
        Scanner kb = new Scanner(System.in);
        while(true){
            try{
                System.out.println("input Row Col:");
                String input = kb.nextLine();
                String[] str = input.split(" ");
                if(str.length!=2){
                    System.out.println("input Row Col");
                    continue;
                }        
                int row = Integer.parseInt(str[0])-1;
                int col = Integer.parseInt(str[1])-1;
                if(board.setTable(row,col)==false){
                    System.out.println("Table not empty,Try again");
                    continue;
                }
                break;
            }catch (Exception e){
                System.out.println("input Row Col");
                continue;
            }            
        }
    }
}
