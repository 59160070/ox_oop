package ox_oop;
public class Player {
    public char name;
    
    public Player(char name){
        this.name = name;        
    }
    
    public char getName(){
        return name;
    }
}